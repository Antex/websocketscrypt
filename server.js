var WebSocketServer = require('websocket').server;
var http = require('http');

var server = http.createServer(function(request, response) {
    // process HTTP request. Since we're writing just WebSockets server
    // we don't have to implement anything.
});
server.listen(1912, function() { });

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

function xorEncode(txt, pass) {
    var ord = []
    var buf = ""

    for (z = 1; z <= 255; z++) {ord[String.fromCharCode(z)] = z}

    for (j = z = 0; z < txt.length; z++) {
        buf += String.fromCharCode(ord[txt.substr(z, 1)] ^ ord[pass.substr(j, 1)])
        j = (j < pass.length) ? j + 1 : 0
    }

    return buf
}

function randomString() {
	var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
	var string_length = 48;
	var randomstring = '';
	
	for (var i=0; i<string_length; i++) {
		var rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum,rnum+1);
	}
	
	return randomstring
}


var sessions = [];
var session_inc = 0;
var key = randomString();

console.log("key: " + key);

function getSession(connection){
	for ( index in sessions ) {
		session = sessions[index];
		
		if(connection == session.connection)
			return session;
	}
}

function eachSession(callback){
	for ( index in sessions ) {
		session = sessions[index];
		
		callback(session);
	}
}

function SessionClass(options){
	this.connection = options.connection;
	this.remoteAddr = this.connection.remoteAddress;
	this.id = session_inc;
	session_inc++;
	sessions.push(this);
}

SessionClass.prototype.send = function(packetnum, data, crypted){
	if(crypted == undefined)
		crypted = true;
		
	var packet = JSON.stringify({
		'pid': packetnum,
		'dat': crypted ? xorEncode(data, key) : data
	});
	
	this.connection.sendUTF(packet);
}

function processMessage(s, msg){

}

// WebSocket server
wsServer.on('request', function(request) {
    var connection = request.accept(null, request.origin);
	//var session_id = connections.indexOf(connection);
	
	session = new SessionClass({
		'connection' : connection
	});
	
	console.log("#" + session.id + ": " + session.remoteAddr + " connected");
	
	session.send(0xDEAD, key, false);
	
	//console.log(sessions);
	
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
			session = getSession(connection);
			console.log("#" + session.id + " message: " + message.utf8Data);
			processMessage(session, message.utf8Data);
        }
    });
	
    connection.on('close', function() {
		session = getSession(connection);
		
		console.log("#" + session.id + ": " + session.remoteAddr + " disconnected");
			
        var index = sessions.indexOf(session);
		
        if (index !== -1) {
            sessions.splice(index, 1);
        }
		
		//console.log(sessions);
    });
});

setInterval(function(){
	new_key = randomString();
	console.log("new key: " + new_key);
	
	eachSession(function(s){
		s.send(0xBEAF, new_key);
	});
	
	key = new_key;	
}, 10000);
	
setInterval(function(){
	eachSession(function(s){
		var d = new Date();
		var t = d.toLocaleTimeString();
		s.send(0xFAFA, t);
	});
}, 50);